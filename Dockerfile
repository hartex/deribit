FROM adoptopenjdk/openjdk11:alpine as builder

COPY target/scala-2.13/deribit-integration-* /deribit-integration.jar

RUN chmod -R 744 /deribit-integration.jar

FROM adoptopenjdk/openjdk11:alpine

COPY --from=builder /deribit-integration.jar deribit-integration.jar
RUN chmod +x deribit-integration.jar

ENTRYPOINT ["java", "-jar", "deribit-integration.jar"]
