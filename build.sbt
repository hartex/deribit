lazy val commonSettings = Seq(
  organization := "me.hartex",
  scalaVersion := "2.13.5",
  description := "Deribit integration assignment",
  scmInfo := Some(
    ScmInfo(
      url("https://github.com/hartex/deribit"),
      "scm:git@github.com:hartex/deribit.git"
    )
  ),
  scalacOptions ++= Seq(
    "-feature",
    "-deprecation",
    "-language:higherKinds",
    "-language:implicitConversions",
    "-Ywarn-unused:-implicits",
    "-Yresolve-term-conflict:object",
    "-Xlint"),
  assembly / test := {},
  Test / fork := true,
  scalafmtOnCompile := true
)

lazy val root = (project in file("."))
  .settings(
    name := "deribit-integration",
    version := "0.1.0",
    assembly / mainClass := Some("me.hartex.deribit.Main"),
    assembly / assemblyJarName := s"${name.value}-${version.value}.jar",
    assembly / assemblyMergeStrategy := {
      case "application.conf" => MergeStrategy.concat
      case PathList("META-INF", "maven", "org.webjars", "swagger-ui", "pom.properties") =>
        MergeStrategy.singleOrError
      case x =>
        val oldStrategy = (assembly / assemblyMergeStrategy).value
        oldStrategy(x)
    },
    libraryDependencies ++=
      Seq(Dependencies.scalaTest % Test, Dependencies.flyway, Dependencies.pureconfig) ++
        Dependencies.doobie ++
        Dependencies.logging ++
        Dependencies.testContainers ++
        Dependencies.tapir ++
        Dependencies.http4s ++
        Dependencies.circe ++
        Dependencies.cats
  )
  .settings(commonSettings: _*)
