import sbt._

object Dependencies {
  val tapirVersion = "0.17.19"
  val http4sVersion = "0.21.24"
  val circeVersion = "0.14.1"
  val testcontainersVersion = "0.39.5"
  val doobieVersion = "0.12.1"

  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.2.8"

  lazy val tapir = Seq(
    "com.softwaremill.sttp.tapir" %% "tapir-core" % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-http4s-server" % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-openapi-docs" % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-openapi-circe-yaml" % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-http4s" % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-redoc-http4s" % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-json-circe" % tapirVersion
  )

  lazy val cats = Seq(
    "org.typelevel" %% "cats-effect" % "2.5.1"
  )

  lazy val http4s = Seq(
    "org.http4s" %% "http4s-blaze-server" % http4sVersion,
    "org.http4s" %% "http4s-blaze-client" % http4sVersion,
    "org.http4s" %% "http4s-circe" % http4sVersion,
    "org.http4s" %% "http4s-dsl" % http4sVersion
  )

  lazy val logging = Seq(
    "ch.qos.logback" % "logback-classic" % "1.2.3",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.9.3"
  )

  lazy val circe = Seq(
    "io.circe" %% "circe-core" % circeVersion,
    "io.circe" %% "circe-generic" % circeVersion,
    "io.circe" %% "circe-parser" % circeVersion
  )

  lazy val doobie = Seq(
    "org.tpolecat" %% "doobie-core" % doobieVersion,
    "org.tpolecat" %% "doobie-hikari" % doobieVersion, // HikariCP transactor.
    "org.tpolecat" %% "doobie-postgres" % doobieVersion, // Postgres driver 42.2.19 + type mappings.
    "org.tpolecat" %% "doobie-specs2" % doobieVersion % "test", // Specs2 support for typechecking statements.
    "org.tpolecat" %% "doobie-scalatest" % doobieVersion % "test" // ScalaTest support for typechecking statements.
  )

  lazy val testContainers = Seq(
    "com.dimafeng" %% "testcontainers-scala-scalatest" % testcontainersVersion % "test",
    "com.dimafeng" %% "testcontainers-scala-postgresql" % testcontainersVersion % "test"
  )

  lazy val flyway = "org.flywaydb" % "flyway-core" % "7.9.1"
  lazy val pureconfig = "com.github.pureconfig" %% "pureconfig" % "0.16.0"
}
