package me.hartex.deribit.db

import cats.effect.IO
import com.typesafe.scalalogging.LazyLogging
import me.hartex.deribit.config.DbConfig
import org.flywaydb.core.Flyway
import org.flywaydb.core.api.configuration.FluentConfiguration
import scala.jdk.CollectionConverters._

object SchemaMigration extends LazyLogging {
  def migrate(config: DbConfig): IO[Int] =
    IO.delay {
      logger.info("Running db schema migrations")
      val count = unsafeMigrate(config)
      logger.info(s"Executed $count migrations")
      count
    }

  private def unsafeMigrate(config: DbConfig): Int = {
    val flywayConfig: FluentConfiguration = Flyway.configure
      .dataSource(
        config.url,
        config.user.orNull,
        config.password.orNull
      )
      .group(true)
      .outOfOrder(false)
      .schemas(config.schema.toList: _*)

    logValidationErrorsIfAny(flywayConfig)
    flywayConfig.load().migrate().migrationsExecuted
  }

  private def logValidationErrorsIfAny(
      configuration: FluentConfiguration
  ): Unit = {
    val validated = configuration
      .ignorePendingMigrations(true)
      .load()
      .validateWithResult()

    if (!validated.validationSuccessful)
      for (error <- validated.invalidMigrations.asScala)
        logger.warn(s"""
             |Failed validation:
             |  - version: ${error.version}
             |  - path: ${error.filepath}
             |  - description: ${error.description}
             |  - errorCode: ${error.errorDetails.errorCode}
             |  - errorMessage: ${error.errorDetails.errorMessage}
        """.stripMargin.strip)
  }
}
