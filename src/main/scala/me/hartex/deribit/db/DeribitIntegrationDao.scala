package me.hartex.deribit.db

import doobie.{ConnectionIO, Read, Update, Write}
import doobie.implicits._
import cats.implicits._
import me.hartex.deribit.model.{AccountBalance, Currency}

trait DeribitIntegrationDao {
  def upsertBalances(balance: List[AccountBalance]): ConnectionIO[Unit]
}

object PgDeribitIntegrationDao extends DeribitIntegrationDao {
  def upsertBalances(balances: List[AccountBalance]): ConnectionIO[Unit] = {
    val query =
      """
        |INSERT INTO account_balances (id, currency, available_balance, reserved_balance)
        |VALUES (?, ?::currency, ?, ?)
        |ON CONFLICT (id, currency)
        |    DO UPDATE SET (available_balance, reserved_balance) = (EXCLUDED.available_balance, EXCLUDED.reserved_balance)""".stripMargin
    Update[AccountBalance](query).updateMany(balances).void
  }

  def getBalances(id: Int): ConnectionIO[List[AccountBalance]] = {
    sql"SELECT id, currency, available_balance, reserved_balance FROM account_balances WHERE id = $id"
      .query[AccountBalance]
      .to[List]
  }

  implicit val currencyWrite: Write[Currency] =
    Write[String].contramap(_.ticker)
  implicit val currencyRead: Read[Currency] =
    Read[String].map(Currency.fromStringUnsafe)
}
