package me.hartex.deribit.db

import java.io.Closeable
import java.util.concurrent.{Executors, TimeUnit}

import cats.effect._
import com.typesafe.scalalogging.LazyLogging
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import doobie._
import doobie.hikari.HikariTransactor
import me.hartex.deribit.config.DbConfig

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class PgClient(private val internalTransactor: HikariTransactor[IO]) extends Closeable {
  def transactor: Transactor[IO] = internalTransactor

  override def close(): Unit = internalTransactor.kernel.close()
}

object PgClient extends LazyLogging {
  val DefaultMaxPoolSize = 10

  def make(
      config: DbConfig
  )(implicit contextShift: ContextShift[IO]): PgClient = {
    val hc = new HikariConfig()
    hc.setDriverClassName(config.driver)
    hc.setJdbcUrl(config.url)
    config.password.foreach(hc.setPassword)
    config.user.foreach(hc.setUsername)
    hc.setMinimumIdle(0)
    hc.setMaximumPoolSize(DefaultMaxPoolSize)
    hc.setSchema(config.schema.getOrElse("public"))
    hc.setConnectionTimeout(Duration(5, TimeUnit.SECONDS).toMillis)

    val connectionEc = Executors.newFixedThreadPool(DefaultMaxPoolSize)
    val workingEc = Executors.newCachedThreadPool()
    val dataSource = new HikariDataSource(hc)

    val pgClient = new PgClient(
      HikariTransactor[IO](
        dataSource,
        ExecutionContext.fromExecutor(connectionEc),
        Blocker.liftExecutionContext(ExecutionContext.fromExecutor(workingEc))
      )
    )

    scala.sys.addShutdownHook {
      logger.debug("Closing Hikari pool")
      pgClient.close()
    }
    pgClient
  }
}
