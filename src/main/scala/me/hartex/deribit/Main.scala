package me.hartex.deribit

import cats.effect.{IO, _}
import cats.syntax.all._
import com.typesafe.scalalogging.LazyLogging
import me.hartex.deribit.api.DeribitIntegrationController
import me.hartex.deribit.client.DeribitClient
import me.hartex.deribit.config.AppConfig
import me.hartex.deribit.core.DeribitIntegrationService
import me.hartex.deribit.db.{PgClient, PgDeribitIntegrationDao, SchemaMigration}
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.server.Router
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.syntax.kleisli._
import sttp.tapir.docs.openapi.OpenAPIDocsInterpreter
import sttp.tapir.openapi.OpenAPI
import sttp.tapir.openapi.circe.yaml._
import sttp.tapir.swagger.http4s.SwaggerHttp4s

import scala.concurrent.ExecutionContext

object Main extends IOApp with LazyLogging {
  implicit val ec: ExecutionContext =
    scala.concurrent.ExecutionContext.Implicits.global

  override def run(args: List[String]): IO[ExitCode] = {
    logger.info("Loading app config")
    val appConfig = AppConfig.loadFromGlobal("app").unsafeRunSync()

    logger.info("Migrating database schema")
    SchemaMigration.migrate(appConfig.db).unsafeRunSync()

    val pgClient = PgClient.make(appConfig.db)
    val httpClient = BlazeClientBuilder[IO](ec)
    val deribitClient =
      new DeribitClient(appConfig.deribitEnv, httpClient.resource)
    val integrationService =
      new DeribitIntegrationService(
        deribitClient,
        pgClient,
        PgDeribitIntegrationDao
      )
    val integrationController = new DeribitIntegrationController(
      integrationService
    )

    // generating the documentation in yml; extension methods come from imported packages
    val openApiDocs: OpenAPI = OpenAPIDocsInterpreter.toOpenAPI(
      integrationController.endpoints,
      "Deribit integration",
      "1.0.0"
    )
    val openApiYml: String = openApiDocs.toYaml

    logger.info("Starting Deribit integration service")

    BlazeServerBuilder[IO](ec)
      .bindHttp(appConfig.port.getOrElse(8080))
      .withHttpApp(
        Router(
          "/" -> (integrationController.routes <+> new SwaggerHttp4s(openApiYml)
            .routes[IO])
        ).orNotFound
      )
      .serve
      .compile
      .drain
      .as(ExitCode.Success)
  }
}
