package me.hartex.deribit

import me.hartex.deribit.core.DeribitIntegrationError
import sttp.model.StatusCode

package object api {
  // will consider all errors as user faults for a purpose of simplification
  def toApiError(error: DeribitIntegrationError): ApiError =
    ApiError(error.message, StatusCode.BadRequest)
}
