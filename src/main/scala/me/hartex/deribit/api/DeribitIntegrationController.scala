package me.hartex.deribit.api

import cats.effect.{Concurrent, ContextShift, IO, Timer}
import cats.syntax.all._
import me.hartex.deribit.core.DeribitIntegrationService
import me.hartex.deribit.model._
import org.http4s.HttpRoutes
import sttp.tapir.server.http4s.{Http4sServerInterpreter, Http4sServerOptions}

class DeribitIntegrationController(val service: DeribitIntegrationService)(implicit
    options: Http4sServerOptions[cats.effect.IO],
    concurrent: Concurrent[IO],
    shift: ContextShift[IO],
    timer: Timer[IO]
) extends DeribitIntegrationEndpoints {

  def getBalances(
      token: AccessToken
  ): IO[Either[ApiError, List[AccountBalance]]] = {
    service.getBalances(token).map(_.left.map(toApiError))
  }

  def getDepositsHistory(
      token: AccessToken,
      limit: Option[Int],
      offset: Option[Int],
      currency: Currency
  ): IO[Either[ApiError, HistoryResponse]] = {
    service
      .getDepositsHistory(token, HistoryRequest(currency, limit.getOrElse(10), offset.getOrElse(0)))
      .map(_.left.map(toApiError))
  }

  def getWithdrawalsHistory(
      token: AccessToken,
      limit: Option[Int],
      offset: Option[Int],
      currency: Currency
  ): IO[Either[ApiError, HistoryResponse]] = {
    service
      .getWithdrawalsHistory(token, HistoryRequest(currency, limit.getOrElse(10), offset.getOrElse(0)))
      .map(_.left.map(toApiError))
  }

  def withdraw(
      token: AccessToken,
      req: WithdrawRequest
  ): IO[Either[ApiError, WithdrawResponse]] = {
    service
      .withdraw(token, req)
      .map(_.left.map(toApiError))
  }

  def transferToSubAccount(
      token: AccessToken,
      req: SubAccountTransferRequest
  ): IO[Either[ApiError, SubAccountTransferResponse]] = {
    service
      .transferToSubAccount(token, req)
      .map(_.left.map(toApiError))
  }

  val getBalancesRoutes: HttpRoutes[IO] =
    Http4sServerInterpreter.toRoutes(getBalancesEndpoint)(getBalances)

  val getDepositsHistoryRoutes: HttpRoutes[IO] =
    Http4sServerInterpreter.toRoutes(getDepositsHistoryEndpoint)(
      (getDepositsHistory _).tupled
    )

  val getWithdrawalsHistoryRoutes: HttpRoutes[IO] =
    Http4sServerInterpreter.toRoutes(getWithdrawalsHistoryEndpoint)(
      (getWithdrawalsHistory _).tupled
    )

  val withdrawRoutes: HttpRoutes[IO] =
    Http4sServerInterpreter.toRoutes(withdrawEndpoint)((withdraw _).tupled)

  val transferToSubAccountRoutes: HttpRoutes[IO] =
    Http4sServerInterpreter.toRoutes(transferToSubAccountEndpoint)(
      (transferToSubAccount _).tupled
    )

  val routes: HttpRoutes[IO] =
    getBalancesRoutes <+> getDepositsHistoryRoutes <+> getWithdrawalsHistoryRoutes <+> withdrawRoutes <+> transferToSubAccountRoutes
}
