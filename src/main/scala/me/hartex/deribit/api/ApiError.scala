package me.hartex.deribit.api

import sttp.model.StatusCode

case class ApiError(msg: String, statusCode: StatusCode)
