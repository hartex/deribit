package me.hartex.deribit.api

import sttp.tapir._
import me.hartex.deribit.model._
import sttp.tapir.Codec.PlainCodec
import sttp.tapir.generic.auto.schemaForCaseClass

trait DeribitIntegrationEndpoints {
  import sttp.tapir.json.circe._
  import me.hartex.deribit.util.JsonConverters._
  import DeribitIntegrationEndpoints.currencyCodec

  val error: EndpointOutput[ApiError] =
    stringBody.and(statusCode).mapTo(ApiError)

  private val baseEndpoint: Endpoint[AccessToken, ApiError, Unit, Any] =
    endpoint
      .in(auth.bearer[String]().mapTo(AccessToken))
      .in("api" / "v1")
      .errorOut(error)

  val getBalancesEndpoint: Endpoint[AccessToken, ApiError, List[AccountBalance], Any] =
    baseEndpoint.get
      .in("balances")
      .out(jsonBody[List[AccountBalance]])
      .description("Return available and reserved account balances")

  val getDepositsHistoryEndpoint: Endpoint[
    (AccessToken, Option[Int], Option[Int], Currency),
    ApiError,
    HistoryResponse,
    Any
  ] =
    baseEndpoint.get
      .in("deposit" / "history")
      .in(query[Option[Int]]("limit"))
      .description("Limit elements for paging")
      .in(query[Option[Int]]("offset"))
      .description("Offset for paging")
      .in(query[Currency]("currency"))
      .description("Currency to query")
      .out(jsonBody[HistoryResponse])
      .description("Return deposit history with paging")

  val getWithdrawalsHistoryEndpoint: Endpoint[
    (AccessToken, Option[Int], Option[Int], Currency),
    ApiError,
    HistoryResponse,
    Any
  ] =
    baseEndpoint.get
      .in("withdraw" / "history")
      .in(query[Option[Int]]("limit"))
      .description("Limit elements for paging")
      .in(query[Option[Int]]("offset"))
      .description("Offset for paging")
      .in(query[Currency]("currency"))
      .description("Currency to query")
      .out(jsonBody[HistoryResponse])
      .description("Return deposit withdrawals with paging")

  val withdrawEndpoint: Endpoint[
    (AccessToken, WithdrawRequest),
    ApiError,
    WithdrawResponse,
    Any
  ] =
    baseEndpoint.post
      .in("withdraw")
      .in(jsonBody[WithdrawRequest])
      .description("Withdraw request")
      .out(jsonBody[WithdrawResponse])
      .description("Withdraw selected currency to some address")

  val transferToSubAccountEndpoint: Endpoint[
    (AccessToken, SubAccountTransferRequest),
    ApiError,
    SubAccountTransferResponse,
    Any
  ] =
    baseEndpoint.post
      .in("transfer" / "subaccount")
      .in(jsonBody[SubAccountTransferRequest])
      .description("Withdraw request")
      .out(jsonBody[SubAccountTransferResponse])
      .description("Withdraw selected currency to some address")

  val endpoints = List(
    getBalancesEndpoint,
    getDepositsHistoryEndpoint,
    getWithdrawalsHistoryEndpoint,
    withdrawEndpoint,
    transferToSubAccountEndpoint
  )
}

object DeribitIntegrationEndpoints {
  implicit val currencyCodec: PlainCodec[Currency] =
    Codec.string.mapDecode({ currencyString =>
      Currency.fromString(currencyString) match {
        case Left(error) =>
          DecodeResult.Error(
            currencyString,
            new IllegalArgumentException(error)
          )
        case Right(value) => DecodeResult.Value(value)
      }
    })(_.ticker)
}
