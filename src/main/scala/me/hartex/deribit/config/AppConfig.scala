package me.hartex.deribit.config

import cats.effect.IO
import com.typesafe.config.{Config, ConfigFactory}
import pureconfig.{ConfigReader, ConfigSource}
import pureconfig.generic.semiauto._

final case class DbConfig(
    url: String,
    driver: String,
    user: Option[String],
    password: Option[String],
    schema: Option[String]
)

final case class AppConfig(
    deribitEnv: DeribitEnvironment,
    port: Option[Int],
    db: DbConfig
)

object AppConfig {
  implicit val deribitEnvReader: ConfigReader[DeribitEnvironment] =
    ConfigReader.fromCursor[DeribitEnvironment] { cursor =>
      cursor.asString.map {
        case "prod" => DeribitEnvironment.Prod
        case _      => DeribitEnvironment.Test
      }
    }

  implicit val dbConfigReader: ConfigReader[DbConfig] = deriveReader[DbConfig]
  implicit val appConfigReader: ConfigReader[AppConfig] =
    deriveReader[AppConfig]

  def loadFromGlobal(configNamespace: String): IO[AppConfig] =
    IO.suspend {
      val config = ConfigFactory.load()
      load(config.getConfig(configNamespace))
    }

  def load(config: Config): IO[AppConfig] =
    IO.delay {
      ConfigSource.fromConfig(config).loadOrThrow[AppConfig]
    }
}
