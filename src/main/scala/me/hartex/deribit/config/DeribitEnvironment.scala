package me.hartex.deribit.config

sealed trait DeribitEnvironment

case object DeribitEnvironment {

  case object Test extends DeribitEnvironment

  case object Prod extends DeribitEnvironment

}
