package me.hartex.deribit.util

import io.circe._
import io.circe.generic.semiauto._
import io.circe.{Decoder, Encoder}
import me.hartex.deribit.model._

object JsonConverters {
  implicit val historyItemDecoder: Decoder[HistoryItem] = (c: HCursor) =>
    for {
      address <- c.downField("address").as[String]
      amount <- c.downField("amount").as[Double]
      currency <- c.downField("currency").as[Currency]
      transactionId <- c.downField("transaction_id").as[String]
      fee <- c.downField("fee").as[Option[Double]]
    } yield HistoryItem(address, amount, currency, transactionId, fee)

  implicit val accountSummaryDecoder: Decoder[AccountSummaryResponse] = (c: HCursor) =>
    for {
      id <- c.downField("id").as[Int]
      equity <- c.downField("equity").as[Double]
      currency <- c.downField("currency").as[Currency]
      available <- c.downField("available_withdrawal_funds").as[Double]
      total <- c.downField("available_funds").as[Double]
    } yield AccountSummaryResponse(id, equity, currency, availableWithdrawalFunds = available, availableFunds = total)

  implicit val currencyEncoder: Encoder[Currency] = Encoder[String].contramap(_.ticker)
  implicit val currencyDecoder: Decoder[Currency] = Decoder[String].emap(Currency.fromString)
  implicit val accountBalanceCodec: Codec[AccountBalance] = deriveCodec
  implicit val historyItemEncoder: Encoder[HistoryItem] = deriveEncoder
  implicit val historyResponseCodec: Codec[HistoryResponse] = deriveCodec
  implicit val withdrawRequestCodec: Codec[WithdrawRequest] = deriveCodec
  implicit val withdrawResponseCodec: Codec[WithdrawResponse] = deriveCodec
  implicit val subAccountTransferCodec: Codec[SubAccountTransferRequest] = deriveCodec
  implicit val subAccountTransferResponseCodec: Codec[SubAccountTransferResponse] = deriveCodec

}
