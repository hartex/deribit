package me.hartex.deribit.core

import doobie.implicits._
import cats.effect.IO
import cats.effect.ContextShift
import cats.implicits._
import me.hartex.deribit.db.{DeribitIntegrationDao, PgClient}
import me.hartex.deribit.client.{DeribitClient, RPCResponse}
import me.hartex.deribit.model._

class DeribitIntegrationService(
    val client: DeribitClient,
    val pgClient: PgClient,
    val integrationDao: DeribitIntegrationDao
)(implicit val context: ContextShift[IO]) {
  import DeribitIntegrationService._

  def getBalances(
      accessToken: AccessToken
  ): IO[Either[DeribitIntegrationError, List[AccountBalance]]] = {
    for {
      responsesList <- List(
        Currency.Bitcoin,
        Currency.Ethereum,
        Currency.TetherDollar
      )
        .map(currency => AccountSummaryRequest(currency))
        .parTraverse(request => client.getAccountSummary(accessToken.token, request))
      balancesList = responsesList
        .traverse(toIntegrationError)
        .map(_.map(_.toAccountBalance))
      result <- balancesList match {
        case error @ Left(_) => IO.pure(error)
        case Right(balances) =>
          integrationDao
            .upsertBalances(balances)
            .transact(pgClient.transactor)
            .map(_ => balancesList)
      }
    } yield result
  }

  def getDepositsHistory(
      accessToken: AccessToken,
      req: HistoryRequest
  ): IO[Either[DeribitIntegrationError, HistoryResponse]] = {
    client
      .getDepositsHistory(accessToken.token, req)
      .map(toIntegrationError)
  }

  def getWithdrawalsHistory(
      accessToken: AccessToken,
      req: HistoryRequest
  ): IO[Either[DeribitIntegrationError, HistoryResponse]] = {
    client
      .getWithdrawalsHistory(accessToken.token, req)
      .map(toIntegrationError)
  }

  def withdraw(
      accessToken: AccessToken,
      req: WithdrawRequest
  ): IO[Either[DeribitIntegrationError, WithdrawResponse]] = {
    client
      .withdraw(accessToken.token, req)
      .map(toIntegrationError)
  }

  def transferToSubAccount(
      accessToken: AccessToken,
      req: SubAccountTransferRequest
  ): IO[Either[DeribitIntegrationError, SubAccountTransferResponse]] = {
    client
      .transferToSubAccount(accessToken.token, req)
      .map(toIntegrationError)
  }
}

object DeribitIntegrationService {
  def toIntegrationError[T](
      response: RPCResponse[T]
  ): Either[DeribitIntegrationError, T] =
    (response.result, response.error) match {
      case (Some(data), None)  => Right(data)
      case (None, Some(error)) => Left(DeribitIntegrationError(error.message))
      case (Some(_), Some(error)) =>
        Left(DeribitIntegrationError(error.message))
      case (None, None) =>
        Left(
          DeribitIntegrationError("Empty response returned from Deribit API")
        )
    }
}
