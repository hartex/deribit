package me.hartex.deribit.core

case class DeribitIntegrationError(message: String)
