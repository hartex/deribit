package me.hartex.deribit.model

import cats.syntax.either._

sealed trait Currency extends Product with Serializable {
  def ticker: String
}

case object Currency {

  case object Bitcoin extends Currency {
    val ticker: String = "BTC"
  }

  case object Ethereum extends Currency {
    val ticker: String = "ETH"
  }

  case object TetherDollar extends Currency {
    val ticker: String = "USDT"
  }

  def fromStringUnsafe(str: String): Currency = {
    fromString(str).getOrElse(
      throw new NoSuchElementException(s"$str is not a valid currency")
    )
  }

  def fromString(str: String): Either[String, Currency] =
    str match {
      case Bitcoin.ticker      => Bitcoin.asRight
      case Ethereum.ticker     => Ethereum.asRight
      case TetherDollar.ticker => TetherDollar.asRight
      case _                   => s"$str is not a valid currency. Expected currency to be one of: BTC, ETH, USDT".asLeft
    }
}
