package me.hartex.deribit.model

case class WithdrawRequest(currency: Currency, address: String, amount: Double)

case class WithdrawResponse(id: String, fee: Double)
