package me.hartex.deribit.model

case class HistoryRequest(currency: Currency, count: Int, offset: Int)

case class HistoryResponse(count: Int, data: List[HistoryItem])
