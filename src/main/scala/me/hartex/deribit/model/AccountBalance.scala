package me.hartex.deribit.model

case class AccountBalance(
    id: Int,
    currency: Currency,
    availableBalance: Double,
    reservedBalance: Double
)
