package me.hartex.deribit.model

case class HistoryItem(
    address: String,
    amount: Double,
    currency: Currency,
    transactionId: String,
    fee: Option[Double] = None
)
