package me.hartex.deribit.model

case class SubAccountTransferRequest(
    currency: Currency,
    amount: Double,
    destination: Int
)

case class SubAccountTransferResponse(id: Int)
