package me.hartex.deribit.model

case class AccountSummaryRequest(currency: Currency, extended: Boolean = true)

case class AccountSummaryResponse(
    id: Int, // account id
    equity: Double,
    currency: Currency,
    availableWithdrawalFunds: Double,
    availableFunds: Double
) {

  def toAccountBalance: AccountBalance =
    AccountBalance(
      id,
      currency,
      availableWithdrawalFunds,
      availableFunds - availableWithdrawalFunds
    )
}
