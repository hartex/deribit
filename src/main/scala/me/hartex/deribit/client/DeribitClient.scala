package me.hartex.deribit.client

import cats.effect.{Concurrent, IO, Resource}
import com.typesafe.scalalogging.LazyLogging
import me.hartex.deribit.config.DeribitEnvironment
import me.hartex.deribit.model._
import org.http4s.client.Client
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.implicits.http4sLiteralsSyntax
import org.http4s.{AuthScheme, Credentials, MediaType, Uri, _}

class DeribitClient(
    val env: DeribitEnvironment,
    val clientResource: Resource[IO, Client[IO]]
)(implicit val concurrent: Concurrent[IO])
    extends Http4sClientDsl[IO]
    with LazyLogging {

  import me.hartex.deribit.client.EntityCodecs._

  val baseApiUrl: Uri = env match {
    case DeribitEnvironment.Test => DeribitClient.testUrl
    case DeribitEnvironment.Prod => DeribitClient.prodUrl
  }

  def getAccountSummary(
      accessToken: String,
      request: AccountSummaryRequest
  ): IO[RPCResponse[AccountSummaryResponse]] = {
    val req = authorizedReq(accessToken).withEntity(
      RPCRequest("private/get_account_summary", request)
    )
    clientResource.use(_.expect[RPCResponse[AccountSummaryResponse]](req))
  }

  val getWithdrawalsHistory: (String, HistoryRequest) => IO[RPCResponse[HistoryResponse]] =
    getHistory("private/get_withdrawals")

  val getDepositsHistory: (String, HistoryRequest) => IO[RPCResponse[HistoryResponse]] =
    getHistory("private/get_deposits")

  private def getHistory(
      method: String
  )(accessToken: String, request: HistoryRequest) = {
    val req = authorizedReq(accessToken).withEntity(RPCRequest(method, request))
    clientResource.use(_.expect[RPCResponse[HistoryResponse]](req))
  }

  def withdraw(
      accessToken: String,
      request: WithdrawRequest
  ): IO[RPCResponse[WithdrawResponse]] = {
    val req = authorizedReq(accessToken).withEntity(
      RPCRequest("private/withdraw", request)
    )
    clientResource.use(_.expect[RPCResponse[WithdrawResponse]](req))
  }

  def transferToSubAccount(
      accessToken: String,
      request: SubAccountTransferRequest
  ): IO[RPCResponse[SubAccountTransferResponse]] = {
    val req = authorizedReq(accessToken).withEntity(
      RPCRequest("private/submit_transfer_to_subaccount", request)
    )
    clientResource.use(_.expect[RPCResponse[SubAccountTransferResponse]](req))
  }

  private def authorizedReq(accessToken: String): Request[IO] =
    Request[IO](method = Method.POST, baseApiUrl)
      .withHeaders(
        headers.Authorization(
          Credentials.Token(AuthScheme.Bearer, accessToken)
        ),
        headers.Accept(MediaType.application.json)
      )
}

object DeribitClient {
  val testUrl = uri"https://test.deribit.com/api/v2"
  val prodUrl = uri"https://deribit.com/api/v2"
}
