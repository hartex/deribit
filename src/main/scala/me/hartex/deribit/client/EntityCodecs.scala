package me.hartex.deribit.client

import cats.effect.IO
import me.hartex.deribit.model._
import org.http4s.{EntityDecoder, EntityEncoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}

object EntityCodecs {

  import io.circe.generic.auto._
  import me.hartex.deribit.util.JsonConverters._

  implicit val accountSummaryEntityDecoder: EntityDecoder[IO, RPCResponse[AccountSummaryResponse]] =
    jsonOf[IO, RPCResponse[AccountSummaryResponse]]

  implicit val accountSummaryEntityEncoder: EntityEncoder[IO, RPCRequest[AccountSummaryRequest]] =
    jsonEncoderOf[IO, RPCRequest[AccountSummaryRequest]]

  implicit val historyEntityDecoder: EntityDecoder[IO, RPCResponse[HistoryResponse]] =
    jsonOf[IO, RPCResponse[HistoryResponse]]

  implicit val historyEntityEncoder: EntityEncoder[IO, RPCRequest[HistoryRequest]] =
    jsonEncoderOf[IO, RPCRequest[HistoryRequest]]

  implicit val withdrawEntityDecoder: EntityDecoder[IO, RPCResponse[WithdrawResponse]] =
    jsonOf[IO, RPCResponse[WithdrawResponse]]

  implicit val withdrawEntityEncoder: EntityEncoder[IO, RPCRequest[WithdrawRequest]] =
    jsonEncoderOf[IO, RPCRequest[WithdrawRequest]]

  implicit val transferToSubAccountEntityDecoder: EntityDecoder[IO, RPCResponse[SubAccountTransferResponse]] =
    jsonOf[IO, RPCResponse[SubAccountTransferResponse]]

  implicit val transferToSubAccountEntityEncoder: EntityEncoder[IO, RPCRequest[SubAccountTransferRequest]] =
    jsonEncoderOf[IO, RPCRequest[SubAccountTransferRequest]]
}
