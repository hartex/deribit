package me.hartex.deribit.client

final case class RPCResponse[R](
    id: Int,
    result: Option[R],
    error: Option[RPCError]
)
