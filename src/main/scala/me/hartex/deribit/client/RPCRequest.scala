package me.hartex.deribit.client

import scala.util.Random

final case class RPCRequest[P](id: Int, method: String, params: P, jsonrpc: String = "2.0")

object RPCRequest {
  def apply[P](method: String, params: P): RPCRequest[P] =
    new RPCRequest(new Random().nextInt(), method, params)
}
