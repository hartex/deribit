package me.hartex.deribit.client

final case class RPCError(code: Int, message: String)
