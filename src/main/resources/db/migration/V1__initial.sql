CREATE TYPE currency AS ENUM ('BTC', 'ETH', 'USDT');

CREATE TABLE account_balances
(
    id                INTEGER          NOT NULL,
    currency          currency         NOT NULL,
    available_balance DOUBLE PRECISION NOT NULL DEFAULT 0,
    reserved_balance  DOUBLE PRECISION NOT NULL DEFAULT 0,
    PRIMARY KEY (id, currency)
);
