package me.hartex.deribit.db

import doobie.implicits._
import com.dimafeng.testcontainers.{ForAllTestContainer, PostgreSQLContainer}
import doobie.implicits.toSqlInterpolator
import me.hartex.deribit.config.DbConfig
import me.hartex.deribit.model.{AccountBalance, Currency}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, OptionValues}
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import org.testcontainers.utility.DockerImageName
import me.hartex.deribit.util.AsyncImplicits.defaultContextShift

class DeribitIntegrationDaoSpec
    extends AnyFunSuite
    with OptionValues
    with Matchers
    with ForAllTestContainer
    with BeforeAndAfterEach
    with BeforeAndAfterAll {

  override val container: PostgreSQLContainer = PostgreSQLContainer(DockerImageName.parse("postgres:12"))

  var dbConfig: DbConfig = _
  var pgClient: PgClient = _

  override def afterStart(): Unit = {
    super.afterStart()
    dbConfig = DbConfig(
      url = container.jdbcUrl,
      driver = container.driverClassName,
      user = Some(container.username),
      password = Some(container.password),
      schema = Some("public")
    )
    pgClient = PgClient.make(dbConfig)
    SchemaMigration.migrate(dbConfig).unsafeRunSync()
  }

  override protected def afterEach(): Unit = {
    super.afterEach()
    sql"TRUNCATE TABLE account_balances CASCADE".update.run
      .transact(pgClient.transactor)
      .unsafeRunSync()
  }

  override def beforeStop(): Unit = {
    if (pgClient != null) pgClient.close()
  }

  test("inserting data that already exists in DB should update it") {
    val initialBalances = List(
      AccountBalance(123, Currency.Bitcoin, 132, 123),
      AccountBalance(123, Currency.Ethereum, 132, 123),
      AccountBalance(123, Currency.TetherDollar, 132, 123)
    )
    PgDeribitIntegrationDao
      .upsertBalances(initialBalances)
      .transact(pgClient.transactor)
      .unsafeRunSync()

    val updatedBalances = initialBalances.head
      .copy(availableBalance = 5, reservedBalance = 0) +: initialBalances.tail
    PgDeribitIntegrationDao
      .upsertBalances(updatedBalances)
      .transact(pgClient.transactor)
      .unsafeRunSync()

    val balances = PgDeribitIntegrationDao
      .getBalances(initialBalances.head.id)
      .transact(pgClient.transactor)
      .unsafeRunSync()

    balances.length shouldBe initialBalances.length
    balances should equal(updatedBalances)
  }

}
