package me.hartex.deribit.util

import java.util.concurrent.Executor

import cats.effect.{Blocker, ContextShift, IO, Timer}

import scala.concurrent.ExecutionContext

object AsyncImplicits {

  /** Global bound thread pool. (Fork-join pool) */
  implicit val globalExecutionContext: ExecutionContext =
    scala.concurrent.ExecutionContext.Implicits.global

  val globalExecutor: Executor = ExecutionContext.global

  implicit final val defaultTimer: Timer[IO] = IO.timer(globalExecutionContext)

  implicit final val defaultContextShift: ContextShift[IO] =
    IO.contextShift(globalExecutionContext)

  final lazy val blocker: Blocker = {
    val (out, close) = Blocker[IO].allocated.unsafeRunSync()
    Runtime.getRuntime.addShutdownHook(new Thread(() => close.unsafeRunSync()))
    out
  }
}
