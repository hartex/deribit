# deribit integration

### Запуск с помощью sbt:
1) нужно стартануть PostgreSQL и создать там базу `deribit_integration`
2) стартуем приложение с помощью sbt run

по пути http://localhost:8080/docs будет доступен swagger с документацией всех эндпоинтов

### Как проверить:
я создал аккаунт на https://test.deribit.com/ и API ключ со следующими креденшалами

client_id `S3ITP6rV`

client_secret `leaaO_yJApDQ4PyuXNzOex1T31ZBYJl09a-ZRSEkbvs`

для тестов можно использовать их

### Авторизация
перед тем как делать какие-либо запросы нужно получить access token для https://test.deribit.com/
для это делаем следующий запрос (креды можно использовать свои или те что уже зашиты в запросе)

```
curl -X GET "https://test.deribit.com/api/v2/public/auth?client_id=S3ITP6rV&client_secret=leaaO_yJApDQ4PyuXNzOex1T31ZBYJl09a-ZRSEkbvs&grant_type=client_credentials" \
-H "Content-Type: application/json"
```

берем `access_token` из json'а тела ответа,
в Swagger нажимаем Authorize и в поле value вводим полученный токен

эндпоинт `/api/v1/withdraw` по идее можно проверить с адресом `1EzgKu5RFz3xJFzjC9zHzYRqvibegB38dG` (он добавлен в deribit в качестве основного для вывода, но висит в статусе "на подтверждении" так что возможно эндпоинт будет работать некорректно пока не подтвердят адрес)

эндпоинт `/api/v1/transfer/subaccount` можно проверить указав в качестве destination `30841` (это id саб аккаунта для креденшелов данных выше)
